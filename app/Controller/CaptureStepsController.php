<?php
class CaptureStepsController extends AppController {
	
	var $autoRender = false;
	public $components = array('RequestHandler');
	
    public function index()
    {
        $captureSteps =  $this->CaptureStep->find(
        	'list', 
        	array(
        		'fields' => array('name', 'label'), 
        		'order' => 'order'
        	)
        );
        echo utf8_decode(json_encode($captureSteps));
    }
}