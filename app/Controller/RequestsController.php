<?php
class RequestsController extends AppController {
	
	var $autoRender = false;
	public $components = array('RequestHandler');
	
	var $uses = array('Request', 'CaptureStep');
	
	public function index()
	{
		$requests =  $this->Request->find('all');
		foreach($requests as $index => $request)
		{
			$created = $requests[$index]['Request']['created'];
			$requests[$index]['Request']['created'] = date('Y-m-d H:i:s', $created->sec);
			
			$modified = $requests[$index]['Request']['modified'];
			$requests[$index]['Request']['modified'] = date('Y-m-d H:i:s', $modified->sec);
			
			$requests[$index]['Request']['next_capture_step'] = self::_getNextCaptureStep($request['Request']['remedy_id']);
		}
		echo utf8_decode(json_encode($requests));
	}
	
	public function add() {
		if ($this->request->is('post'))
		{
			# Upload logic design image
			if(!empty($this->request->data['upload_logic_design']))
			{
				$request = $this->Request->findByRemedyId($this->request->data['remedy_id']);
				$request['Request']['logic_design'] = Router::url('/img/', true) . self::_uploadFile();
				$this->request->data = $request;
			}
			
			if(!empty($this->request->data['Request']['Servers']))
			{
				foreach($this->request->data['Request']['Servers'] as $index => $server)
				{
					if(empty($server['_id']))
					{
						$_id = new MongoId();
						$this->request->data['Request']['Servers'][$index] = array_merge(
							array('_id' => $_id->{'$id'}),
							$server
						);
					}
				}
			}
			
			//print_r($this->request->data);
			if ($this->Request->save($this->request->data))
			{
				$this->response->statusCode(200); // OK
				echo utf8_decode(
					json_encode(
						array_merge(
							$this->request->data, 
							array(
								'next_capture_step' => self::_getNextCaptureStep($this->request->data['Request']['remedy_id'])
							)
						)
					)
				);
			} 
			else
			{
				$this->response->statusCode(400); // Bad Request
				echo utf8_decode(json_encode($this->Request->validationErrors));
			}
			/*
			'message' => $message,
			'_serialize' => array('message')
			*/
		}
	}
	
	public function view($remedy_id)
	{
		$request = $this->Request->findByRemedyId($remedy_id);
		if(empty($request))
		{
			throw new NotFoundException(__d('app', 'Invalid request'));
		}
		else
		{
			$nextStep = self::_getNextCaptureStep($request['Request']['remedy_id']);
			if(empty($request['Request']['CaptureStep'][$nextStep]))
			{
				$request['Request']['CaptureStep'][$nextStep] = 'Pending';
			}
			
			echo utf8_decode(json_encode($request));
		}
	}
	
	
	protected function _getNextCaptureStep($remedy_id)
	{
		$steps = array();
		$request = $this->Request->findByRemedyId($remedy_id);
		foreach($request['Request']['CaptureStep'] as $step => $status)
		{
			if($status == 'Completed') $steps[] = $step;
		}
		
		$next = $this->CaptureStep->find(
			'first',
			array(
				'conditions' => array(
					'name' => array('$nin' => $steps)
				),
				'order' => array('order')
			)
		);
		return  isset($next['CaptureStep']['name']) ? $next['CaptureStep']['name']: null;
	}
	
	protected function _uploadFile()
	{
		App::import('Vendor', 'fCore', array('file' => 'flourishlib/fCore.php'));
		App::import('Vendor', 'fFile', array('file' => 'flourishlib/fFile.php'));
		App::import('Vendor', 'fImage', array('file' => 'flourishlib/fImage.php'));
		App::import('Vendor', 'fException', array('file' => 'flourishlib/fException.php'));
		App::import('Vendor', 'fUnexpectedException', array('file' => 'flourishlib/fUnexpectedException.php'));
		App::import('Vendor', 'fEnvironmentException', array('file' => 'flourishlib/fEnvironmentException.php'));
		App::import('Vendor', 'fExpectedException', array('file' => 'flourishlib/fExpectedException.php'));
		App::import('Vendor', 'fValidationException', array('file' => 'flourishlib/fValidationException.php'));
		App::import('Vendor', 'fFilesystem', array('file' => 'flourishlib/fFilesystem.php'));
		App::import('Vendor', 'fDirectory', array('file' => 'flourishlib/fDirectory.php'));
		App::import('Vendor', 'fUpload', array('file' => 'flourishlib/fUpload.php'));
		
		$uploader = new fUpload();
		$uploader->setMIMETypes(
			array(
				'image/gif',
				'image/jpeg',
				'image/pjpeg',
				'image/png'
			),
			'The file uploaded is not an image'
		);
		
		try
		{
			$file = $uploader->move(IMAGES, 'logic_design');
			foreach($file->fImage as $item)
			{
				if(isset($item->fImage[0]))
				{
					return Configure::read('Config.logicDesignPath'). $item->getName();
				}
			}
		} 
		catch (Exception $e) 
		{
			return array('error' => $e->getMessage());
		}
		die;
	}
}