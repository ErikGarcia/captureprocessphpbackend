<?php
class Request extends AppModel {
 
  public $validate = array(
  	'remedy_id' => array(
        'rule' => 'notBlank',
		'message' => 'Remedy ID is required'
    ),
    'system_name' => array(
        'rule' => 'notBlank',
		'message' => 'System name is required'
    )
  ); 
  
	public function beforeValidate($options = array())
	{
		if(isset($this->data['Request']['Servers']))
		{
			foreach($this->data['Request']['Servers'] as $iindex => $server)
			{
				if(!empty($server['Users']))
				{
					foreach($server['Users'] as $jindex => $user)
					{
						if(isset($user['access_type']) && empty($user['access_type']))
						{
							$this->invalidate("Server.Users['{$jindex}'].access_type", __("Access type is required"));
						}
					}	
				}
				else 
				{
					$this->invalidate('general-error-message', __("Provide at least one application user"));
				}
			}
		}
	}
}