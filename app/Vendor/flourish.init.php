<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

function __autoload($class_name)
{
    // Customize this to your root Flourish directory
    $flourish_root = $_SERVER['DOCUMENT_ROOT'] . '/bootstrap-fileinput/script/flourishlib/';
    
    $file = $flourish_root . $class_name . '.php';

    if (file_exists($file)) {
        include $file;
        return;
    }
    
    throw new Exception('The class ' . $file . ' could not be loaded');
}
